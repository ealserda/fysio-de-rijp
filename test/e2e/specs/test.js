// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'Login page e2e tests': function (browser) {
    const devServer = browser.globals.devServerURL

    browser
      .url(`${devServer}/login`)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('#loginForm')
      .assert.containsText('h1', 'Login Fysio De Rijp')
      .assert.value('#submitBtn', 'Inloggen')
      .setValue("#email", "frank@gmail.com")
      .setValue("#password", "welcome01")
      .click("#submitBtn")
      .end()

  }
}
