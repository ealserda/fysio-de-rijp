[![buddy pipeline](https://app.buddy.works/ealserda/fysio/pipelines/pipeline/135416/badge.svg?token=3828fd780b06bed70aa1ab061a1a0de62e189e750b9783a103799762b8b57d8c "buddy pipeline")](https://app.buddy.works/ealserda/fysio/pipelines/pipeline/135416)

# fysio

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
