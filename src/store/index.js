import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import patient from './patient'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    user,
    patient
  },
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters: {
  }
})
