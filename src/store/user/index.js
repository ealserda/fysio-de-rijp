import Vue from 'vue'
import LocalStore from '../local-store'

export default {
  state: {
    user: LocalStore('user'),
    storage: {total: null, used: null},
    signedUp: false
  },
  mutations: {
    setUser (state, payload) {
      if (payload) {
        state.user = payload.therapist
        state.user.jwt = payload.token.access_token
      } else {
        state.user = payload
      }
      LocalStore('user', state.user)
    }
  },
  actions: {
    signUserIn ({commit}, payload) {
      return new Promise((resolve, reject) => {
        Vue.http.post(`${process.env.API_SERVER}/auth/login`, {email: payload.email, password: payload.password}).then(response => {
          if (response.body) {
            commit('setUser', response.body)
            resolve()
          } else {
            reject(new Error('Email is niet geregistreerd'))
          }
        }, error => {
          reject(new Error('Inloggegeven incorrect', error))
        })
      })
    },
    logout ({commit}) {
      commit('setUser', null)
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
}
