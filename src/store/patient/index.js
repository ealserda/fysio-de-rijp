import Vue from 'vue'

export default {
  state: {
    patients: [],
    patient: {}
  },
  mutations: {
    setPatients (state, payload) {
      state.patients = payload
    },
    setPatient (state, payload) {
      state.patient = payload
    },
    addPatient (state, payload) {
      state.patients.push(payload)
    }
  },
  actions: {
    loadPatients ({commit}) {
      return new Promise((resolve, reject) => {
        Vue.http.get(`${process.env.API_SERVER}/patients`, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.getters.user.jwt}`
          }
        }).then(response => {
          commit('setPatients', response.body.data)
          resolve()
        }, error => {
          if (error.status === 401) {
            this.dispatch('logout')
          }
          reject(error)
        })
      })
    },
    loadPatientDetail ({commit}, payload) {
      return new Promise((resolve, reject) => {
        Vue.http.get(`${process.env.API_SERVER}/patients/${payload}`, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.getters.user.jwt}`
          }
        }).then(response => {
          commit('setPatient', response.body.data)
          resolve()
        }, error => {
          if (error.status === 401) {
            this.dispatch('logout')
          }
          reject(error)
        })
      })
    },
    addPatient ({commit}, payload) {
      return new Promise((resolve, reject) => {
        Vue.http.post(`${process.env.API_SERVER}/patients`, payload, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.getters.user.jwt}`
          }
        }).then(response => {
          commit('addPatient', response.body.data)
          resolve(response.body.data)
        }).catch(error => {
          if (error.status === 401) {
            this.dispatch('logout')
          }
          reject(error)
        })
      })
    },
    addFysio ({commit}, payload) {
      return new Promise((resolve, reject) => {
        Vue.http.post(`${process.env.API_SERVER}/userRegister`, payload, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.getters.user.jwt}`
          }
        }).then(response => {
          console.log(response.body.data)
          resolve(response.body.data)
        }).catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },
    editPatient ({commit}, payload) {
      return new Promise((resolve, reject) => {
        Vue.http.patch(`${process.env.API_SERVER}/patients/${payload.id}`, payload, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.getters.user.jwt}`
          }
        }).then(response => {
          resolve(response.body.data)
        }).catch(error => {
          if (error.status === 401) {
            this.dispatch('logout')
          }
          reject(error)
        })
      })
    }
  },
  getters: {
    patients (state) {
      return state.patients
    },
    patient (state) {
      return state.patient
    }
  }
}
