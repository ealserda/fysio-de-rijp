import Vue from 'vue'
import Router from 'vue-router'
import Signin from '@/components/user/Signin'
import PatientOverview from '@/components/patient/Overview'
import PatientDetail from '@/components/patient/Detail'
import PatientNew from '@/components/patient/New'
import FysioNew from '@/components/fysio/New'
import PatientEdit from '@/components/patient/Edit'
import PatientSettings from '@/components/patient/Settings'
import PageNotFound from '@/components/404'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Signin',
      component: Signin
    },
    {
      path: '/',
      name: 'PatientOverview',
      beforeEnter: AuthGuard,
      component: PatientOverview
    },
    {
      path: '/fysio/new',
      name: 'FysioNew',
      beforeEnter: AuthGuard,
      component: FysioNew
    },
    {
      path: '/patient/new',
      name: 'PatientNew',
      beforeEnter: AuthGuard,
      component: PatientNew
    },
    {
      path: '/patient/:patientId',
      name: 'PatientDetail',
      beforeEnter: AuthGuard,
      component: PatientDetail
    },
    {
      path: '/patient/:patientId/edit',
      name: 'PatientEdit',
      beforeEnter: AuthGuard,
      component: PatientEdit
    },
    {
      path: '/patient/:patientId/settings',
      name: 'PatientSettings',
      beforeEnter: AuthGuard,
      component: PatientSettings
    },
    { path: '*', component: PageNotFound }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  mode: 'history'
})
