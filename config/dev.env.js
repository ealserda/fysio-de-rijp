'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_SERVER: '"https://api.fysio-de-rijp.nl/api"',
  HOST: '0.0.0.0'
})
